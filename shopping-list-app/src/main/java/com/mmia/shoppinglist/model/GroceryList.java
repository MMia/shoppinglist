package com.mmia.shoppinglist.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "GROCERY_LIST")
public class GroceryList {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(min = 1, max = 30)
	private String name;

	@OneToMany(mappedBy ="groceryList", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Product> products;

	@Column(name = "CREATION_TIME")
	private LocalDate creationTime;

	@Column(name = "LAST_UPDATE")
	private LocalDate lastUpdate;
	
	private GroceryList() {
	}

	public GroceryList(final String name) {
		this.name = name;
		this.products = new ArrayList<Product>();
		this.creationTime = LocalDate.now();
	}
	
	// Util methods for hibernate
	// used to synchronize both sides of the bidirectional association grocery list and product
	
	public void addProduct(Product product) {
		products.add(product);
		product.setGroceryList(this);
	}
	
	public void removeProduct(Product product) {
		products.remove(product);
		product.setGroceryList(null);
	}

	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public LocalDate getCreateTime() {
		return creationTime;
	}
	public void setCreateTime(final LocalDate createTime) {
		this.creationTime = createTime;
	}
	public LocalDate getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(final LocalDate lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public List<Product> getProducts() {
		return products;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroceryList other = (GroceryList) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

}
