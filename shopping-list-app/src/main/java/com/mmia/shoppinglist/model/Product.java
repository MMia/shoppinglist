package com.mmia.shoppinglist.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="PRODUCT")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "id", insertable = false, updatable = false)
	private GroceryList groceryList;
	
	@NotNull
	private String name;
	
	@NotNull
	private int quantity;
	
	@NotNull
	private ProductStatus productStatus;

	@NotNull
	private LocalDateTime addDate;

	private LocalDateTime buyDate;
	
	public Product() {
	}

	public Product(GroceryList groceryList, String name, int quantity) {
		this.groceryList = groceryList;
		this.name = name;
		this.quantity = quantity;
		this.productStatus = ProductStatus.OPEN;
		this.addDate = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}
	
	public GroceryList getGroceryList() {
		return groceryList;
	}
	public void setGroceryList(GroceryList groceryList) {
		this.groceryList = groceryList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public ProductStatus getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(ProductStatus productStatus) {
		this.productStatus = productStatus;
	}
	public LocalDateTime getAddDate() {
		return addDate;
	}
	public void setAddDate(LocalDateTime addDate) {
		this.addDate = addDate;
	}
	public LocalDateTime getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(LocalDateTime buyDate) {
		this.buyDate = buyDate;
	}
	
}
