package com.mmia.shoppinglist.repository;

import javax.persistence.EntityManager;

import com.mmia.shoppinglist.model.GroceryList;

public class GroceryListRepository {
	EntityManager em;

	public GroceryList add(GroceryList groceryList) {
		em.persist(groceryList);
		return groceryList;
	}

	public GroceryList findById(Long id) {
		return id == null ? null : em.find(GroceryList.class, id);
	}

	public void update(GroceryList finded) {
		em.merge(finded);
	}
}
