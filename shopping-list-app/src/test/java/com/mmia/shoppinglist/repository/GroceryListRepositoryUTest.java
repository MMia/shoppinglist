package com.mmia.shoppinglist.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mmia.shoppinglist.model.GroceryList;
import com.mmia.shoppinglist.testsutils.DBCommandTransactionalExecutor;

public class GroceryListRepositoryUTest {
	EntityManager em;

	private EntityManagerFactory emf;
	private GroceryListRepository glRepository;
	private DBCommandTransactionalExecutor dbCommandTransactionalExecutor;
	private GroceryList groceryList;

	@Before
	public void onBeforeTest() {
		emf = Persistence.createEntityManagerFactory("shoppingPU");
		em = emf.createEntityManager();
		glRepository = new GroceryListRepository();
		glRepository.em = em;
		dbCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);
		groceryList = new GroceryList("TestList");
	}

	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}

	@Test
	public void addGroceryListAndFindIt() {
		Long groceryListAddedId = dbCommandTransactionalExecutor.executeDBCommand(() -> {
			return glRepository.add(groceryList).getId();
		});
		assertNotNull(groceryListAddedId);

		GroceryList finded = glRepository.findById(groceryListAddedId);
		assertNotNull(finded);
		assertEquals(groceryList.getName(), finded.getName());
	}

	@Test
	public void findGroceryListNotFound() {
		GroceryList gl = glRepository.findById(100L);
		assertNull(gl);
	}

	@Test
	public void findGroceryListByIdWithNullId() {
		GroceryList gl = glRepository.findById(null);
		assertNull(gl);
	}

	@Test
	public void updateGroceryList() {
		String newName = "New name";
		Long groceryListAddedId = dbCommandTransactionalExecutor.executeDBCommand(() -> {
			return glRepository.add(groceryList).getId();
		});
		GroceryList finded = glRepository.findById(groceryListAddedId);
		assertEquals(groceryList.getName(), finded.getName());
		finded.setName(newName);

		dbCommandTransactionalExecutor.executeDBCommand(() -> {
			glRepository.update(finded);
			return null;
		});

		GroceryList afterUpdate = glRepository.findById(finded.getId());
		assertEquals(newName, afterUpdate.getName());
	}


}
