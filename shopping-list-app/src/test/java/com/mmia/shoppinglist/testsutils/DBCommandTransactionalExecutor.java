package com.mmia.shoppinglist.testsutils;

import javax.persistence.EntityManager;

import org.junit.Ignore;

@Ignore
public class DBCommandTransactionalExecutor {
	private EntityManager em;

	public DBCommandTransactionalExecutor(EntityManager em) {
		this.em = em;
	}

	public <T> T executeDBCommand(DBCommand<T> dbCommand) {
		try {
			em.getTransaction().begin();
			T  toReturn = dbCommand.execute();
			em.getTransaction().commit();
			em.clear();
			return toReturn;
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new IllegalStateException(e);
		}
	}
}
