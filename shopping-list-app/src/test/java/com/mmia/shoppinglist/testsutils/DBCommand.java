package com.mmia.shoppinglist.testsutils;

import org.junit.Ignore;

@Ignore
public interface DBCommand<T> {
	T execute();
}
